<?php

namespace Drupal\redirect;

use Drupal\Core\PathProcessor\PathProcessorManager;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Request;

class RedirectPathNormalizer {

  /**
   * @var \Drupal\Core\PathProcessor\PathProcessorManager
   */
  protected $pathProcessorManager;

  public function __construct(PathProcessorManager $pathProcessorManager) {
    $this->pathProcessorManager = $pathProcessorManager;
  }

  public function normalize($path, Request $request = NULL, RequestContext $requestContext = NULL) {
    $path = '/' . ltrim($path, '/');
    if (!$request) {
      $request = Request::create($path);
    }
    $path = $this->pathProcessorManager->processInbound($request->getPathInfo(), $request);
    $path = ltrim($path, '/');
    if (isset($requestContext)) {
      $requestContext->fromRequest($request);
    }
    return $path;
  }

}
