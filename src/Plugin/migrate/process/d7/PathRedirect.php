<?php

/**
 * @file
 * Contains \Drupal\redirect\Plugin\migrate\process\d7\PathRedirect.
 */

namespace Drupal\redirect\Plugin\migrate\process\d7;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_path_redirect"
 * )
 */
class PathRedirect extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  public function __construct(array $configuration, string $plugin_id, $plugin_definition, PathValidatorInterface $pathValidator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathValidator = $pathValidator;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Transform the field as required for an iFrame field.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (UrlHelper::isExternal($value[0])) {
      // Use external URLs as is.
      $uri = $value[0];
    }
    else {
      $path = '/' . $value[0];
      // By default, the link is an unknown internal.
      $uri = 'internal:' . $path;
      // Then try making it into an entity path.
      $url = $this->pathValidator->getUrlIfValidWithoutAccessCheck($path);
      if ($url && $url->isRouted()) {
        // Test for entity URLs.
        if (count($url->getRouteParameters()) === 1) {
          foreach ($url->getRouteParameters() as $entityType => $entityId) {
            if ($url->getRouteName() === "entity.$entityType.canonical") {
              $uri = "entity:$entityType/$entityId";
            }
          }
        }
      }
    }

    // Check if there are options.
    if (!empty($value[1])) {
      // Check if there is a query.
      $options = unserialize($value[1]);
      if (!empty($options['query'])) {
        // Add it to the end of the url.
        $uri .= '?' . http_build_query($options['query']);
      }
      if (!empty($options['fragment'])) {
        $uri .= '#' . $options['fragment'];
      }
    }

    return $uri;
  }

}
